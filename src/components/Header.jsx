import React from 'react';

const Header = () => {
    return (
        <div className="header">
            <div className="header_box">
                <div className="ico">

                </div>
                <div className="heading">
                    <h1>Библиотека сказок</h1>
                </div>
                <div>
                    <nav >
                        <ul className="main_menu">
                            <li>
                                <a>Главная</a>
                            </li>
                            <li>
                                <a>Авторы</a>
                            </li>
                            <li>
                                <a>О нас</a>
                            </li>
                            <li>
                                <a>Контакты</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    );
};

export default Header;