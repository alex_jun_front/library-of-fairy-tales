import React from 'react';

class Text extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            x: 0,
            selectedItem:   {id: 0, name: '', year: 0, publisher: '', pages: '' }, //отвечает за отрисовку выбранного элемента
            newItem:        {id: 0, name: '', year: 0, publisher: '', pages: '' },
            changeItem:     {id: 0, name: '', year: 0, publisher: '', pages: '' },
            condition:      {id: 0, name: '', year: 0, publisher: '', pages: '' },
            //отвечает за отрисовку элементов меню
            list_books: [{
                "id": 100,
                "name": "Кот в сапогах",
                "year": 2001,
                "publisher": "Дрофа",
                "pages": 200
            }, {
                "id": 101,
                "name": "Три порося",
                "year": 2002,
                "publisher": "Олол",
                "pages": 240
            }, {
                "id": 102,
                "name": "Колобок",
                "year": 2001,
                "publisher": "Дрофа и ко",
                "pages": 300
            }]
        }
    }
    //функция срабатывает по клику и ищет выбранный элемент в массиве
    viewContent = (e) => {
        let findid = e.target.dataset.id; //переменной присваиваем id выбранного элемента
        for(let i=0; i < this.state.list_books.length; i++ )
        {
           if (findid == this.state.list_books[i].id){
               this.setState({selectedItem: this.state.list_books[i], changeItem: this.state.list_books[i]})
           }
        }
    }


   // changeContent = (e) => {
    //    let slc_item = e.target.dataset.id;
   //     this.setState([this.state.list_books[slc_item] = this.state.newItem]);
   //     console.log(this.state.list_books);
   // }

      /*    <div className={this.state.selectedItem.id == item.id ? "item active" : "item"}
                                 key={item.id} data-id={item.id}
                                 onClick={this.viewContent}>
    *Выбираем название класса в замисимости от того выбра элемент или нет
    *
    <div className="cont_text">
                   { this.state.selectedItem.id == 0    проверяем является ли значение по умолчанию или нет
                       ? <div>Выберите книгу:</div>     если значение по умолчанию то заглушка
                       : <div>                          если выбран элемент выводим его данные
                           <div><h3>{this.state.selectedItem.id}</h3></div>
                           <div><h2>{this.state.selectedItem.name}</h2></div>
                       </div>
                   }
               </div>
    * */

    //--------Блок функций изменяющих существующий элемент в массиве-------------
    changeID = (e) => {
        this.setState({changeItem: {...this.state.changeItem, id: Number(e.target.value)},
            newItem: {...this.state.newItem, id: Number(e.target.value)}});
    }
    changeName = (e) => {
        this.setState({changeItem: {...this.state.changeItem, name: e.target.value},
            newItem: {...this.state.newItem, name: e.target.value}});
    }
    changeYear = (e) => {
        this.setState({changeItem: {...this.state.changeItem, year: Number(e.target.value)},
            newItem: {...this.state.newItem, year: Number(e.target.value)}});
    }
    changePublisher = (e) => {
        this.setState({changeItem: {...this.state.changeItem, publisher: e.target.value},
            newItem: {...this.state.newItem, publisher: e.target.value}});
    }
    changePages = (e) => {
        this.setState({changeItem: {...this.state.changeItem, pages: e.target.value},
            newItem: {...this.state.newItem, pages: e.target.value}});
    }
    changeContent = () => {
        let change_item = this.state.changeItem.id;
        let newit = [...this.state.list_books];
        for(let i=0; i < this.state.list_books.length; i++ )
        {
            if (change_item == this.state.list_books[i].id){
                newit[i] = this.state.changeItem
                this.setState({list_books: newit, selectedItem: this.state.changeItem})
            }
        }
    }
    //-----------------------------------------------------------------------------


    //-------------Блок функций создающих новый элемент в массиве(новый)-----------------
    changeValueId = () => {
        if (this.state.x === 1 ) {
            return this.state.condition.id
        }
        else  return   this.state.changeItem.id
    }
    changeValueName = () => {
        if (this.state.x === 1 ) return this.state.condition.name
        else   return   this.state.changeItem.name

    }
    changeValueYear = () => {
        if (this.state.x === 1 ) return this.state.condition.year
        else  return   this.state.changeItem.year
    }
    changeValuePublisher = () => {
        if (this.state.x === 1 ) return this.state.condition.publisher
        else  return   this.state.changeItem.publisher
    }
    changeValuePages = () => {
        if (this.state.x === 1 ) return this.state.condition.pages
        else  return   this.state.changeItem.pages
    }

    createContent = () => {
        this.setState({list_books: [...this.state.list_books, this.state.changeItem]})
        this.setState({x: 0})
    }

    addInput = () => {
        if (this.state.x == 0 )
            this.setState({x: 1})
        else
            this.createContent()
    }

    viewCons = () => {
        console.log("changeItem: ")
        console.log(this.state.changeItem)
        console.log("newItem: " )
        console.log(this.state.newItem)
    }

    //-----------------------------------------------------------------------------

    render() {
        return (
            <div className="box">
                <div className="wrap">
                    <div className="cont_menu" >
                        {this.state.list_books.map(item => {
                            return (
                                <div className={this.state.selectedItem.id == item.id ? "item active" : "item"}
                                     key={item.id} data-id={item.id}
                                     onClick={this.viewContent}>
                                    {item.name}
                                </div>
                            )
                        })}
                    </div>
                   <div className="cont_text">
                       { this.state.selectedItem.id == 0
                           ? <div>Выберите книгу:</div>
                           : <div>
                               <div><h3>{this.state.selectedItem.id}</h3></div>
                               <div><h2>{this.state.selectedItem.name}</h2></div>
                               <div><h3>{this.state.selectedItem.year}</h3></div>
                               <div><h3>{this.state.selectedItem.publisher}</h3></div>
                               <div><h3>{this.state.selectedItem.pages}</h3></div>
                           </div>
                       }
                   </div>

                    <div className="func_block">

                        <div>
                            <div className="edit_item">
                                <fieldset>
                                    <legend>Редактировать:</legend>
                                    <p>Введите id:</p>
                                    <input className="unic_number"  onChange={this.changeID}
                                           value={this.changeValueId()}
                                           type="text" />
                                    <p>Название сказки:</p>
                                    <input className="name_story" ref="myRef" onChange={this.changeName}
                                           value={this.changeValueName()}
                                           type="text" />
                                    <p>Год издания:</p>
                                    <input className="year_edition" onChange={this.changeYear}
                                           value={this.changeValueYear()}
                                           type="text"/>
                                    <p>Издатель:</p>
                                    <input className="pub_title" onChange={this.changePublisher}
                                           value={this.changeValuePublisher()}
                                           type="text"/>
                                    <p>Количество страниц:</p>
                                    <input className="num_pages" onChange={this.changePages}
                                           value={this.changeValuePages()}
                                           type="text"/><br/><br/>
                                    <button onClick={this.changeContent}>Изменить</button><br/><br/>
                                    <button onClick={this.addInput} >Добавить новый</button><p>{this.state.x}</p>
                                    <button onClick={this.viewCons} >Консоль</button>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Text;
